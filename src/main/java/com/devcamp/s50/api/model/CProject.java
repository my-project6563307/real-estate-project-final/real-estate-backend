package com.devcamp.s50.api.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "project")
public class CProject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Project name không được để trống")
    @Column(name = "name", length = 200)
    private String name;

    @Column(name = "address", length = 1000)
    private String address;

    @Lob
    @Column(name = "slogan", columnDefinition = "mediumtext")
    private String slogan;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "acreage", length = 19)
    private BigDecimal acreage;

    @Column(name = "construct_area", length = 19)
    private BigDecimal constructArea;

    @Column(name = "num_block")
    private Long numBlock;

    @Column(name = "num_floors", length = 500)
    private String numFloors;

    @Column(name = "num_apartment")
    private Integer numApartment;

    @Column(name = "apartmentt_area", length = 500)
    private String apartmenttArea;

    @ManyToOne
    @JoinColumn(name = "investor")
    private CInvestor investor;

    @ManyToOne
    @JoinColumn(name = "construction_contractor")
    private CConstructionContractor constructionContractor;

    @ManyToOne
    @JoinColumn(name = "design_unit")
    private CDesignUnit designUnit;

    @ManyToOne
    @JoinColumn(name = "utilities")
    private CUtilities utilities;

    @ManyToOne
    @JoinColumn(name = "region_link")
    private CRegionLink regionLink;

    @Column(name = "photo")
    private String photo;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lng")
    private Double lng;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "ward_id")
    private CWard ward;

    @ManyToOne
    @JoinColumn(name = "street_id")
    private CStreet street;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CMasterLayout> masterLayout;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CRealEstate> realestates;
    // constructor

    public CProject() {
    }

    public CProject(Integer id, @NotEmpty(message = "Project name không được để trống") String name, String address,
            String slogan, String description, BigDecimal acreage, BigDecimal constructArea, Long numBlock,
            String numFloors, Integer numApartment, String apartmenttArea, CInvestor investor,
            CConstructionContractor constructionContractor, CDesignUnit designUnit, CUtilities utilities,
            CRegionLink regionLink, String photo, Double lat, Double lng, CProvince province, CDistrict district,
            CWard ward, CStreet street, Set<CMasterLayout> masterLayout, Set<CRealEstate> realestates) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.slogan = slogan;
        this.description = description;
        this.acreage = acreage;
        this.constructArea = constructArea;
        this.numBlock = numBlock;
        this.numFloors = numFloors;
        this.numApartment = numApartment;
        this.apartmenttArea = apartmenttArea;
        this.investor = investor;
        this.constructionContractor = constructionContractor;
        this.designUnit = designUnit;
        this.utilities = utilities;
        this.regionLink = regionLink;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;
        this.masterLayout = masterLayout;
        this.realestates = realestates;
    }
    // getter and setter

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public Long getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Long numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Integer getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(Integer numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmenttArea() {
        return apartmenttArea;
    }

    public void setApartmenttArea(String apartmenttArea) {
        this.apartmenttArea = apartmenttArea;
    }

    public CInvestor getInvestor() {
        return investor;
    }

    public void setInvestor(CInvestor investor) {
        this.investor = investor;
    }

    public CConstructionContractor getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(CConstructionContractor constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public CDesignUnit getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(CDesignUnit designUnit) {
        this.designUnit = designUnit;
    }

    public CUtilities getUtilities() {
        return utilities;
    }

    public void setUtilities(CUtilities utilities) {
        this.utilities = utilities;
    }

    public CRegionLink getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(CRegionLink regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CWard getWard() {
        return ward;
    }

    public void setWard(CWard ward) {
        this.ward = ward;
    }

    public CStreet getStreet() {
        return street;
    }

    public void setStreet(CStreet street) {
        this.street = street;
    }

    public Set<CMasterLayout> getMasterLayout() {
        return masterLayout;
    }

    public void setMasterLayout(Set<CMasterLayout> masterLayout) {
        this.masterLayout = masterLayout;
    }

    public Set<CRealEstate> getRealestates() {
        return realestates;
    }

    public void setRealestates(Set<CRealEstate> realestates) {
        this.realestates = realestates;
    }

}
