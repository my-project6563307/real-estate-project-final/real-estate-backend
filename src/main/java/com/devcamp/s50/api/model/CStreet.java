package com.devcamp.s50.api.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "street")
public class CStreet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Street name không được để trống")
    @Column(name = "name", nullable = false)
    private String name;

    @NotEmpty(message = "Street prefix không được để trống")
    @Size(min = 2, message = "prefix phải có tối thiểu 2 kí tự")
    @Column(name = "prefix", nullable = false)
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private CDistrict district;

    @OneToMany(mappedBy = "street", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CRealEstate> realestates;

    // Constructor
    public CStreet() {
    }

    // Getter and setter

    public CStreet(Integer id, @NotEmpty(message = "Street name không được để trống") String name,
            @NotEmpty(message = "Street prefix không được để trống") @Size(min = 2, message = "prefix phải có tối thiểu 2 kí tự") String prefix,
            CProvince province, CDistrict district, Set<CRealEstate> realestates) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.district = district;
        this.realestates = realestates;
    }

    public Integer getId() {
        return id;
    }

    public Set<CRealEstate> getRealestates() {
        return realestates;
    }

    public void setRealestates(Set<CRealEstate> realestates) {
        this.realestates = realestates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

}
