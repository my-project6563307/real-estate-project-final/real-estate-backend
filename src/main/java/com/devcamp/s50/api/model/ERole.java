package com.devcamp.s50.api.model;

public enum ERole {
    ROLE_CUSTOMER,
    ROLE_HOME_SELLER,
    ROLE_ADMIN
}
