package com.devcamp.s50.api.security.services;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.s50.api.model.CEmployees;
import com.devcamp.s50.api.repository.IEmployeesRepository;
import com.devcamp.s50.api.security.jwt.JwtUtils;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  IEmployeesRepository employeesRepository;

  @Autowired
  private JwtUtils jwtUtils;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    CEmployees employee = employeesRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

    return UserDetailsImpl.build(employee);
  }

  public CEmployees whoami(HttpServletRequest request) {
    
    String token = resolveToken(request);

    String username = jwtUtils.getUserNameFromJwtToken(token);

    return employeesRepository.findByUsername(username).get();
  }

  private String resolveToken(HttpServletRequest req) {
    String bearerToken = req.getHeader("Authorization");
    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
      return bearerToken.substring(7);
    }
    return null;
  }
}

