package com.devcamp.s50.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.api.model.CInvestor;

public interface IInvestorRepository extends JpaRepository<CInvestor, Integer>{
    
}
