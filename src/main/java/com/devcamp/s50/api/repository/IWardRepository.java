package com.devcamp.s50.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CWard;

@Service
public interface IWardRepository extends JpaRepository<CWard, Integer> {

}
