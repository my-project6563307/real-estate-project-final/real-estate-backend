package com.devcamp.s50.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.api.model.CEmployees;
import com.devcamp.s50.api.model.ERole;

public interface IEmployeesRepository extends JpaRepository<CEmployees, Integer>{

    List<CEmployees> findByRolesName(ERole role);

    Optional<CEmployees> findById(Long id);

    void deleteById(Long id);

    Optional<CEmployees> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
