package com.devcamp.s50.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.api.model.CDesignUnit;

public interface IDesignUnitRepository extends JpaRepository<CDesignUnit, Integer>{
    
}
