package com.devcamp.s50.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.api.model.CMasterLayout;

public interface IMasterLayoutRepository extends JpaRepository<CMasterLayout, Integer>{
    
}
