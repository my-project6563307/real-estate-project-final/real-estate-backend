package com.devcamp.s50.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.s50.api.model.CRealEstate;
import com.devcamp.s50.api.model.ERole;

public interface IRealEstateRepository extends JpaRepository<CRealEstate, Integer>{

List<CRealEstate> findByEmployeeId(Long employeeId);

List<CRealEstate> findByEmployeeRolesName(ERole name);

@Query(value = "SELECT * FROM `realestate` WHERE request LIKE "+":request" 
+" AND province_id LIKE " + ":province_id"
+" AND price BETWEEN :price1 AND :price2 AND is_accepted = 1"
+" AND acreage BETWEEN :acreage1 AND :acreage2 ", nativeQuery = true)
List<CRealEstate> findByRequest(@Param("request") String request,
 @Param("province_id") String provinceId,
 @Param("price1") String price1, @Param("price2") String price2,
 @Param("acreage1") String acreage1, @Param("acreage2") String acreage2);
}
