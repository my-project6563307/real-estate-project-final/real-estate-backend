package com.devcamp.s50.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.api.model.CLocations;

public interface ILocationsRepository extends JpaRepository<CLocations, Integer>{
    
}
