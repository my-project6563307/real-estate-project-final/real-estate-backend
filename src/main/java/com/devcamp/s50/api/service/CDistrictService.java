package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CDistrict;
import com.devcamp.s50.api.model.CProvince;
import com.devcamp.s50.api.repository.IDistrictRepository;
import com.devcamp.s50.api.repository.IProvinceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Service
public class CDistrictService {
    @Autowired
    IProvinceRepository provinceRepository;
    @Autowired
    IDistrictRepository districtRepository;

    // Get all districts
    public ResponseEntity<List<CDistrict>> getAllDistricts(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CDistrict> districtList = new ArrayList<>();
            districtRepository.findAll(pageWithElements).forEach(districtList::add);

            return new ResponseEntity<>(districtList,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // get district by district id
    public ResponseEntity<Object> geDistrictById(int id){
        try {
            Optional<CDistrict> districtData = districtRepository.findById(id);
            if(districtData.isPresent()){
                CDistrict districtFound = districtData.get();

                return new ResponseEntity<>(districtFound,HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // get district by province id
    public ResponseEntity<Set<CDistrict>> getDistrictByProvinceId(int provinceId) {
        try {
            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);

            if (provinceData.isPresent()) {
                Set<CDistrict> districtFound = new HashSet<>();
                districtFound = provinceData.get().getDistricts();
                return new ResponseEntity<>(districtFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create a district of a province
    public ResponseEntity<Object> createDistrict(int provinceId, CDistrict pDistrict) {
        try {
            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
            if (provinceData.isPresent()) {
                CDistrict newDistrict = new CDistrict();
                newDistrict.setPrefix(pDistrict.getPrefix());
                newDistrict.setName(pDistrict.getName());
                newDistrict.setProvince(pDistrict.getProvince());

                CProvince _province = provinceData.get();
                newDistrict.setProvince(_province);
                CDistrict saveDistrict = districtRepository.save(newDistrict);
                return new ResponseEntity<>(saveDistrict, HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified district: " + e.getCause().getCause().getMessage());
        }
    }

    // update a district by district id
    public ResponseEntity<Object> updateDistrictById(int provinceId, int id, CDistrict pDistrict) {
        try {
            Optional<CDistrict> districtData = districtRepository.findById(id);
            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);

            if (districtData.isPresent()) {
                CDistrict updateDistrict = districtData.get();
                updateDistrict.setPrefix(pDistrict.getPrefix());
                updateDistrict.setName(pDistrict.getName());
                updateDistrict.setProvince(provinceData.get());

                return new ResponseEntity<>(districtRepository.save(updateDistrict), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    //delete a district by id
    public ResponseEntity<Object> deleteDistrictById(int id){
        try {
            Optional<CDistrict> districtData = districtRepository.findById(id);
            if (districtData.isPresent()){
                districtRepository.deleteById(id);

                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
