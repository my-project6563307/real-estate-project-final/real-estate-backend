package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CConstructionContractor;
import com.devcamp.s50.api.repository.IConstructionContractorRepository;

@Service
public class CConstructionContractorService {
    @Autowired
    IConstructionContractorRepository constructionContractorRepository;

    // Get all construction contractor
    public ResponseEntity<List<CConstructionContractor>> getAllConstructionContractors(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CConstructionContractor> constructionContractorList = new ArrayList<>();
            constructionContractorRepository.findAll(pageWithElements).forEach(constructionContractorList::add);

            return new ResponseEntity<>(constructionContractorList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get construction contractor by id 
    public ResponseEntity<Object> getConstructionContractorById (int id){
        try {
            Optional<CConstructionContractor> ConstructionContractorData = constructionContractorRepository.findById(id);

            if(ConstructionContractorData.isPresent()){
                CConstructionContractor ConstructionContractorFound = ConstructionContractorData.get();

                return new ResponseEntity<>(ConstructionContractorFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new construction contractor
    public ResponseEntity<Object> createConstructionContractor(CConstructionContractor pConstructionContractor){
        try {
            CConstructionContractor newConstructionContractor = new CConstructionContractor();

                newConstructionContractor.setName(pConstructionContractor.getName());
                newConstructionContractor.setDescription(pConstructionContractor.getDescription());
                newConstructionContractor.setProjects(pConstructionContractor.getProjects());
                newConstructionContractor.setAddress(pConstructionContractor.getAddress());
                newConstructionContractor.setPhone(pConstructionContractor.getPhone());
                newConstructionContractor.setPhone2(pConstructionContractor.getPhone2());
                newConstructionContractor.setFax(pConstructionContractor.getFax());
                newConstructionContractor.setEmail(pConstructionContractor.getEmail());
                newConstructionContractor.setWebsite(pConstructionContractor.getWebsite());
                newConstructionContractor.setNote(pConstructionContractor.getNote());

                CConstructionContractor savedConstructionContractor = constructionContractorRepository.save(newConstructionContractor);

                return new ResponseEntity<>(savedConstructionContractor, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified street: " + e.getCause().getCause().getMessage());
        }
    }

    //Update a construction contractor by Id
    public ResponseEntity<Object> updateConstructionContractorById(int id, CConstructionContractor pConstructionContractor){
        try {
            Optional<CConstructionContractor> ConstructionContractorData = constructionContractorRepository.findById(id);

            if (ConstructionContractorData.isPresent()){

                CConstructionContractor ConstructionContractorFound = ConstructionContractorData.get();

                ConstructionContractorFound.setName(pConstructionContractor.getName());
                ConstructionContractorFound.setDescription(pConstructionContractor.getDescription());
                ConstructionContractorFound.setProjects(pConstructionContractor.getProjects());
                ConstructionContractorFound.setAddress(pConstructionContractor.getAddress());
                ConstructionContractorFound.setPhone(pConstructionContractor.getPhone());
                ConstructionContractorFound.setPhone2(pConstructionContractor.getPhone2());
                ConstructionContractorFound.setFax(pConstructionContractor.getFax());
                ConstructionContractorFound.setEmail(pConstructionContractor.getEmail());
                ConstructionContractorFound.setWebsite(pConstructionContractor.getWebsite());
                ConstructionContractorFound.setNote(pConstructionContractor.getNote());

                return new ResponseEntity<>(constructionContractorRepository.save(ConstructionContractorFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //delete construction contractor by Id
     public ResponseEntity<Object> deleteConstructionContractorById(int id){
        try {
            Optional<CConstructionContractor> ConstructionContractorData = constructionContractorRepository.findById(id);

            if(ConstructionContractorData.isPresent()){
                
                constructionContractorRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
