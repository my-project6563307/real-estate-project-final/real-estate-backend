package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CDistrict;
import com.devcamp.s50.api.model.CEmployees;
import com.devcamp.s50.api.model.CProject;
import com.devcamp.s50.api.model.CProvince;
import com.devcamp.s50.api.model.CRealEstate;
import com.devcamp.s50.api.model.CStreet;
import com.devcamp.s50.api.model.CWard;
import com.devcamp.s50.api.model.ERole;
import com.devcamp.s50.api.repository.ICustomerRepository;
import com.devcamp.s50.api.repository.IDistrictRepository;
import com.devcamp.s50.api.repository.IEmployeesRepository;
import com.devcamp.s50.api.repository.IProjectRepository;
import com.devcamp.s50.api.repository.IProvinceRepository;
import com.devcamp.s50.api.repository.IRealEstateRepository;
import com.devcamp.s50.api.repository.IStreetRepository;
import com.devcamp.s50.api.repository.IWardRepository;
import com.devcamp.s50.api.security.services.UserDetailsServiceImpl;

@Service
public class CRealEstateService {
    @Autowired
    IRealEstateRepository realEstateRepository;
    @Autowired
    IProvinceRepository provinceRepository;
    @Autowired
    IDistrictRepository districtRepository;
    @Autowired
    IWardRepository wardRepository;
    @Autowired
    IStreetRepository streetRepository;
    @Autowired
    IProjectRepository projectRepository; 
    @Autowired
    ICustomerRepository customerRepository;
    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;
    @Autowired
    IEmployeesRepository employeesRepository;

    // Get all Real estates
    public ResponseEntity<List<CRealEstate>> getAllRealEstate(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CRealEstate> realEstateList = new ArrayList<>();
            realEstateRepository.findAll(pageWithElements).forEach(realEstateList::add);

            return new ResponseEntity<>(realEstateList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Get posted real estate
    public ResponseEntity<List<CRealEstate>> getPostedRealEstate(
        String page,
        String size){
            try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CRealEstate> realEstateList = new ArrayList<>();

            realEstateRepository.findAll(pageWithElements).forEach(realEstate -> {
                if(realEstate.isAccepted()){
                    realEstateList.add(realEstate);
                }
            });
            return new ResponseEntity<>(realEstateList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get posted real estate by id
    public ResponseEntity<CRealEstate> getPostedRealEstateById(Integer id){
            try {

            Optional<CRealEstate> realEstateData =  realEstateRepository.findById(id);

                if(realEstateData.isPresent() && realEstateData.get().isAccepted()){

                    return new ResponseEntity<>(realEstateData.get(), HttpStatus.OK);
                }
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get Estates by id
    public ResponseEntity<Object> getDesignUnitById (HttpServletRequest request, Integer id){
        try {

            Optional<CRealEstate> realEstateData = realEstateRepository.findById(id);

            if(realEstateData.isPresent()){

                CRealEstate realEstateFound = realEstateData.get();

                return new ResponseEntity<>(realEstateFound, HttpStatus.OK);
            }
            return new ResponseEntity<>("You can not access the data or data are no longer existed",HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Create a realestate
    public ResponseEntity<Object> createRealEstate(HttpServletRequest request, CRealEstate pRealEstate, Integer provinceId, Integer districtId, Integer wardId,
     Integer streetId, Integer projectId){
        try {

            CEmployees employeesFound = userDetailsServiceImpl.whoami(request);
            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            Optional<CWard> wardData = wardRepository.findById(wardId);
            Optional<CStreet> streetData = streetRepository.findById(streetId);
            Optional<CProject> projectData = projectRepository.findById(projectId);
            
            CRealEstate newRealEstate = new CRealEstate();

            newRealEstate.setProvince(provinceData.get());
            newRealEstate.setDistrict(districtData.get());
            newRealEstate.setWard(wardData.get());
            newRealEstate.setStreet(streetData.get());
            newRealEstate.setProject(projectData.get());
            newRealEstate.setEmployee(employeesFound);
            
            newRealEstate.setAddress(pRealEstate.getAddress());
            newRealEstate.setTitle(pRealEstate.getTitle());
            newRealEstate.setType(pRealEstate.getType());
            newRealEstate.setRequest(pRealEstate.getRequest());
            newRealEstate.setPrice(pRealEstate.getPrice());
            newRealEstate.setPriceMin(pRealEstate.getPriceMin());
            newRealEstate.setPriceTime(pRealEstate.getPriceTime());

            newRealEstate.setDate_create(new Date());
            newRealEstate.setAcreage(pRealEstate.getAcreage());
            newRealEstate.setDirection(pRealEstate.getDirection());
            newRealEstate.setTotal_floors(pRealEstate.getTotal_floors());
            newRealEstate.setNumber_floors(pRealEstate.getNumber_floors());
            newRealEstate.setBath(pRealEstate.getBath());
            newRealEstate.setApart_code(pRealEstate.getApart_code());
            newRealEstate.setWall_area(pRealEstate.getWall_area());
            newRealEstate.setBedroom(pRealEstate.getBedroom());
            newRealEstate.setBalcony(pRealEstate.getBalcony());
            newRealEstate.setLandscape_view(pRealEstate.getLandscape_view());
            newRealEstate.setApart_loca(pRealEstate.getApart_loca());
            newRealEstate.setApart_type(pRealEstate.getApart_type());

            newRealEstate.setPrice_rent(pRealEstate.getPrice_rent());
            newRealEstate.setReturn_rate(pRealEstate.getReturn_rate());
            newRealEstate.setLegal_doc(pRealEstate.getLegal_doc());
            newRealEstate.setDescription(pRealEstate.getDescription());
            newRealEstate.setWidth_y(pRealEstate.getWidth_y());
            newRealEstate.setLong_x(pRealEstate.getLong_x());
            newRealEstate.setStreet_house(pRealEstate.getStreet_house());
            newRealEstate.setFSBO(pRealEstate.getFSBO());
            newRealEstate.setView_num(pRealEstate.getView_num());
            newRealEstate.setShape(pRealEstate.getShape());
            newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
            newRealEstate.setFurniture_type(pRealEstate.getFurniture_type());

            newRealEstate.setAdjacent_facade_num(pRealEstate.getAdjacent_facade_num());
            newRealEstate.setAdjacent_road(pRealEstate.getAdjacent_road());
            newRealEstate.setAlley_min_width(pRealEstate.getAlley_min_width());
            newRealEstate.setAdjacent_alley_min_width(pRealEstate.getAdjacent_alley_min_width());
            newRealEstate.setFactor(pRealEstate.getFactor());
            newRealEstate.setStructure(pRealEstate.getStructure());
            newRealEstate.setDTSXD(pRealEstate.getDTSXD());
            newRealEstate.setCLCL(pRealEstate.getCLCL());
            newRealEstate.setCTXD_price(pRealEstate.getCTXD_price());
            newRealEstate.setCTXD_value(pRealEstate.getCTXD_value());
            newRealEstate.setPhoto(pRealEstate.getPhoto());
            newRealEstate.setLat(pRealEstate.getLat());
            newRealEstate.setLng(pRealEstate.getLng());
            newRealEstate.setAccepted(false);

            CRealEstate savedRealEstate = realEstateRepository.save(newRealEstate);

            return new ResponseEntity<>(savedRealEstate, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Realestate: " + e.getCause().getCause().getMessage());
        }
    }

    //Update a realestate by id
    public ResponseEntity<Object> updateRealEstateById(HttpServletRequest request,Integer id, CRealEstate pRealEstate,
        Integer provinceId, Integer districtId, Integer wardId,
        Integer streetId, Integer projectId, Long employeeId){

        try {
            Optional<CRealEstate> realEstateData = realEstateRepository.findById(id);

            if(realEstateData.isPresent()){

            Optional<CEmployees> employeeData = employeesRepository.findById(employeeId);
            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            Optional<CWard> wardData = wardRepository.findById(wardId);
            Optional<CStreet> streetData = streetRepository.findById(streetId);
            Optional<CProject> projectData = projectRepository.findById(projectId);
            
            CRealEstate newRealEstate = realEstateData.get();

            newRealEstate.setProvince(provinceData.get());
            newRealEstate.setDistrict(districtData.get());
            newRealEstate.setWard(wardData.get());
            newRealEstate.setStreet(streetData.get());
            newRealEstate.setProject(projectData.get());
            newRealEstate.setEmployee(employeeData.get());
            
            newRealEstate.setAddress(pRealEstate.getAddress());
            newRealEstate.setTitle(pRealEstate.getTitle());
            newRealEstate.setType(pRealEstate.getType());
            newRealEstate.setRequest(pRealEstate.getRequest());
            newRealEstate.setPrice(pRealEstate.getPrice());
            newRealEstate.setPriceMin(pRealEstate.getPriceMin());
            newRealEstate.setPriceTime(pRealEstate.getPriceTime());

            newRealEstate.setAcreage(pRealEstate.getAcreage());
            newRealEstate.setDirection(pRealEstate.getDirection());
            newRealEstate.setTotal_floors(pRealEstate.getTotal_floors());
            newRealEstate.setNumber_floors(pRealEstate.getNumber_floors());
            newRealEstate.setBath(pRealEstate.getBath());
            newRealEstate.setApart_code(pRealEstate.getApart_code());
            newRealEstate.setWall_area(pRealEstate.getWall_area());
            newRealEstate.setBedroom(pRealEstate.getBedroom());
            newRealEstate.setBalcony(pRealEstate.getBalcony());
            newRealEstate.setLandscape_view(pRealEstate.getLandscape_view());
            newRealEstate.setApart_loca(pRealEstate.getApart_loca());
            newRealEstate.setApart_type(pRealEstate.getApart_type());

            newRealEstate.setPrice_rent(pRealEstate.getPrice_rent());
            newRealEstate.setReturn_rate(pRealEstate.getReturn_rate());
            newRealEstate.setLegal_doc(pRealEstate.getLegal_doc());
            newRealEstate.setDescription(pRealEstate.getDescription());
            newRealEstate.setWidth_y(pRealEstate.getWidth_y());
            newRealEstate.setLong_x(pRealEstate.getLong_x());
            newRealEstate.setStreet_house(pRealEstate.getStreet_house());
            newRealEstate.setFSBO(pRealEstate.getFSBO());
            newRealEstate.setView_num(pRealEstate.getView_num());
            newRealEstate.setShape(pRealEstate.getShape());
            newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
            newRealEstate.setFurniture_type(pRealEstate.getFurniture_type());

            newRealEstate.setAdjacent_facade_num(pRealEstate.getAdjacent_facade_num());
            newRealEstate.setAdjacent_road(pRealEstate.getAdjacent_road());
            newRealEstate.setAlley_min_width(pRealEstate.getAlley_min_width());
            newRealEstate.setAdjacent_alley_min_width(pRealEstate.getAdjacent_alley_min_width());
            newRealEstate.setFactor(pRealEstate.getFactor());
            newRealEstate.setStructure(pRealEstate.getStructure());
            newRealEstate.setDTSXD(pRealEstate.getDTSXD());
            newRealEstate.setCLCL(pRealEstate.getCLCL());
            newRealEstate.setCTXD_price(pRealEstate.getCTXD_price());
            newRealEstate.setCTXD_value(pRealEstate.getCTXD_value());
            newRealEstate.setLat(pRealEstate.getLat());
            newRealEstate.setLng(pRealEstate.getLng());
            newRealEstate.setAccepted(pRealEstate.isAccepted());
            newRealEstate.setPhoto(pRealEstate.getPhoto());

            CRealEstate savedRealEstate = realEstateRepository.save(newRealEstate);

            return new ResponseEntity<>(savedRealEstate, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    //Delete realestate by id and request
    public ResponseEntity<Object> deleteRealEstateById(HttpServletRequest request,Integer id){
        try {
            List<CRealEstate> realestateList = getRealEstateByRequest(request);
            Optional<CRealEstate> realEstateData = realEstateRepository.findById(id);
            if(realEstateData.isPresent() && realestateList.contains(realEstateData.get())){

                realEstateRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>("You can not access the data or data are no longer existed", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get real estate list by request with jwt token
    public List<CRealEstate> getRealEstateByRequest(HttpServletRequest request){
        
            CEmployees employeesFound = userDetailsServiceImpl.whoami(request);

            List<CRealEstate> realEstatesList = new ArrayList<>();

            employeesFound.getRoles().forEach(Role -> {
                if (Role.getName() == ERole.ROLE_CUSTOMER){

                    List<CRealEstate> RealestateList = realEstateRepository.findByEmployeeId(employeesFound.getId());

                    realEstatesList.addAll(RealestateList);
                }
                if (Role.getName() == ERole.ROLE_HOME_SELLER){

                    List<CRealEstate> homeSellerRealestateList = realEstateRepository.findByEmployeeId(employeesFound.getId());
                    List<CRealEstate> customerRealestateLisst = realEstateRepository.findByEmployeeRolesName(ERole.ROLE_CUSTOMER);

                    realEstatesList.addAll(homeSellerRealestateList);
                    realEstatesList.addAll(customerRealestateLisst);
                }
                if (Role.getName() == ERole.ROLE_ADMIN){

                    List<CRealEstate> allRealestateList = realEstateRepository.findAll();

                    realEstatesList.addAll(allRealestateList);
                }
            });
        return realEstatesList;
    }

    //update real estate of customer role 
    public ResponseEntity<Object> updateRealEstateForCustomerRole(HttpServletRequest request, Integer realestateId, CRealEstate pRealEstate,
    Integer provinceId, Integer districtId, Integer wardId,
    Integer streetId, Integer projectId){

        try {
            List<CRealEstate> realestateList = getRealEstateByRequest(request);

            Optional<CRealEstate> realEstateData = realEstateRepository.findById(realestateId);

            if (realEstateData.isPresent() && realestateList.contains(realEstateData.get())){
                Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
                Optional<CDistrict> districtData = districtRepository.findById(districtId);
                Optional<CWard> wardData = wardRepository.findById(wardId);
                Optional<CStreet> streetData = streetRepository.findById(streetId);
                Optional<CProject> projectData = projectRepository.findById(projectId);
                
                CRealEstate newRealEstate = realEstateData.get();

                newRealEstate.setAddress(pRealEstate.getAddress());
                newRealEstate.setProvince(provinceData.get());
                newRealEstate.setDistrict(districtData.get());
                newRealEstate.setWard(wardData.get());
                newRealEstate.setStreet(streetData.get());
                newRealEstate.setProject(projectData.get());
                
                newRealEstate.setTitle(pRealEstate.getTitle());
                newRealEstate.setType(pRealEstate.getType());
                newRealEstate.setRequest(pRealEstate.getRequest());
                newRealEstate.setPrice(pRealEstate.getPrice());
                newRealEstate.setPriceMin(pRealEstate.getPriceMin());
                newRealEstate.setPriceTime(pRealEstate.getPriceTime());

                newRealEstate.setDate_create(new Date());
                newRealEstate.setAcreage(pRealEstate.getAcreage());
                newRealEstate.setDirection(pRealEstate.getDirection());
                newRealEstate.setTotal_floors(pRealEstate.getTotal_floors());
                newRealEstate.setNumber_floors(pRealEstate.getNumber_floors());
                newRealEstate.setBath(pRealEstate.getBath());
                newRealEstate.setApart_code(pRealEstate.getApart_code());
                newRealEstate.setWall_area(pRealEstate.getWall_area());
                newRealEstate.setBedroom(pRealEstate.getBedroom());
                newRealEstate.setBalcony(pRealEstate.getBalcony());
                newRealEstate.setLandscape_view(pRealEstate.getLandscape_view());
                newRealEstate.setApart_loca(pRealEstate.getApart_loca());
                newRealEstate.setApart_type(pRealEstate.getApart_type());

                newRealEstate.setPrice_rent(pRealEstate.getPrice_rent());
                newRealEstate.setReturn_rate(pRealEstate.getReturn_rate());
                newRealEstate.setLegal_doc(pRealEstate.getLegal_doc());
                newRealEstate.setDescription(pRealEstate.getDescription());
                newRealEstate.setWidth_y(pRealEstate.getWidth_y());
                newRealEstate.setLong_x(pRealEstate.getLong_x());
                newRealEstate.setStreet_house(pRealEstate.getStreet_house());
                newRealEstate.setFSBO(pRealEstate.getFSBO());
                newRealEstate.setView_num(pRealEstate.getView_num());
                newRealEstate.setShape(pRealEstate.getShape());
                newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
                newRealEstate.setFurniture_type(pRealEstate.getFurniture_type());

                newRealEstate.setAdjacent_facade_num(pRealEstate.getAdjacent_facade_num());
                newRealEstate.setAdjacent_road(pRealEstate.getAdjacent_road());
                newRealEstate.setAlley_min_width(pRealEstate.getAlley_min_width());
                newRealEstate.setAdjacent_alley_min_width(pRealEstate.getAdjacent_alley_min_width());
                newRealEstate.setFactor(pRealEstate.getFactor());
                newRealEstate.setStructure(pRealEstate.getStructure());
                newRealEstate.setDTSXD(pRealEstate.getDTSXD());
                newRealEstate.setCLCL(pRealEstate.getCLCL());
                newRealEstate.setCTXD_price(pRealEstate.getCTXD_price());
                newRealEstate.setCTXD_value(pRealEstate.getCTXD_value());
                newRealEstate.setLat(pRealEstate.getLat());
                newRealEstate.setLng(pRealEstate.getLng());
                newRealEstate.setAccepted(false);
                newRealEstate.setPhoto(pRealEstate.getPhoto());

                CRealEstate savedRealEstate = realEstateRepository.save(newRealEstate);

            return new ResponseEntity<>(savedRealEstate, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update real estate of seller role
    public ResponseEntity<Object> updateRealEstateForSellerRole(HttpServletRequest request, Integer realestateId, CRealEstate pRealEstate,
    Integer provinceId, Integer districtId, Integer wardId,
    Integer streetId, Integer projectId){

        try {
            List<CRealEstate> realestateList = getRealEstateByRequest(request);

            Optional<CRealEstate> realEstateData = realEstateRepository.findById(realestateId);

            if (realEstateData.isPresent() && realestateList.contains(realEstateData.get())){
                Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
                Optional<CDistrict> districtData = districtRepository.findById(districtId);
                Optional<CWard> wardData = wardRepository.findById(wardId);
                Optional<CStreet> streetData = streetRepository.findById(streetId);
                Optional<CProject> projectData = projectRepository.findById(projectId);
                
                CRealEstate newRealEstate = realEstateData.get();

                newRealEstate.setAddress(pRealEstate.getAddress());
                newRealEstate.setProvince(provinceData.get());
                newRealEstate.setDistrict(districtData.get());
                newRealEstate.setWard(wardData.get());
                newRealEstate.setStreet(streetData.get());
                newRealEstate.setProject(projectData.get());
                
                newRealEstate.setTitle(pRealEstate.getTitle());
                newRealEstate.setType(pRealEstate.getType());
                newRealEstate.setRequest(pRealEstate.getRequest());
                newRealEstate.setPrice(pRealEstate.getPrice());
                newRealEstate.setPriceMin(pRealEstate.getPriceMin());
                newRealEstate.setPriceTime(pRealEstate.getPriceTime());

                newRealEstate.setDate_create(new Date());
                newRealEstate.setAcreage(pRealEstate.getAcreage());
                newRealEstate.setDirection(pRealEstate.getDirection());
                newRealEstate.setTotal_floors(pRealEstate.getTotal_floors());
                newRealEstate.setNumber_floors(pRealEstate.getNumber_floors());
                newRealEstate.setBath(pRealEstate.getBath());
                newRealEstate.setApart_code(pRealEstate.getApart_code());
                newRealEstate.setWall_area(pRealEstate.getWall_area());
                newRealEstate.setBedroom(pRealEstate.getBedroom());
                newRealEstate.setBalcony(pRealEstate.getBalcony());
                newRealEstate.setLandscape_view(pRealEstate.getLandscape_view());
                newRealEstate.setApart_loca(pRealEstate.getApart_loca());
                newRealEstate.setApart_type(pRealEstate.getApart_type());

                newRealEstate.setPrice_rent(pRealEstate.getPrice_rent());
                newRealEstate.setReturn_rate(pRealEstate.getReturn_rate());
                newRealEstate.setLegal_doc(pRealEstate.getLegal_doc());
                newRealEstate.setDescription(pRealEstate.getDescription());
                newRealEstate.setWidth_y(pRealEstate.getWidth_y());
                newRealEstate.setLong_x(pRealEstate.getLong_x());
                newRealEstate.setStreet_house(pRealEstate.getStreet_house());
                newRealEstate.setFSBO(pRealEstate.getFSBO());
                newRealEstate.setView_num(pRealEstate.getView_num());
                newRealEstate.setShape(pRealEstate.getShape());
                newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
                newRealEstate.setFurniture_type(pRealEstate.getFurniture_type());

                newRealEstate.setAdjacent_facade_num(pRealEstate.getAdjacent_facade_num());
                newRealEstate.setAdjacent_road(pRealEstate.getAdjacent_road());
                newRealEstate.setAlley_min_width(pRealEstate.getAlley_min_width());
                newRealEstate.setAdjacent_alley_min_width(pRealEstate.getAdjacent_alley_min_width());
                newRealEstate.setFactor(pRealEstate.getFactor());
                newRealEstate.setStructure(pRealEstate.getStructure());
                newRealEstate.setDTSXD(pRealEstate.getDTSXD());
                newRealEstate.setCLCL(pRealEstate.getCLCL());
                newRealEstate.setCTXD_price(pRealEstate.getCTXD_price());
                newRealEstate.setCTXD_value(pRealEstate.getCTXD_value());
                newRealEstate.setLat(pRealEstate.getLat());
                newRealEstate.setLng(pRealEstate.getLng());
                newRealEstate.setAccepted(pRealEstate.isAccepted());
                newRealEstate.setPhoto(pRealEstate.getPhoto());

                CRealEstate savedRealEstate = realEstateRepository.save(newRealEstate);

            return new ResponseEntity<>(savedRealEstate, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
