package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CAddressMap;
import com.devcamp.s50.api.repository.IAddressMapRepository;


@Service
public class CAddressMapService {
    @Autowired
    IAddressMapRepository addressMapRepository;

    // Get all address map
    public ResponseEntity<List<CAddressMap>> getAllAddressMap(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CAddressMap> addressMapList = new ArrayList<>();
            
            addressMapRepository.findAll(pageWithElements).forEach(addressMapList::add);

            return new ResponseEntity<>(addressMapList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Get address map by id
    public ResponseEntity<Object> getAddressMapById (int id){
        try {
            Optional<CAddressMap> addressMapData = addressMapRepository.findById(id);
            if(addressMapData.isPresent()){
                CAddressMap addressMapFound = addressMapData.get();

                return new ResponseEntity<>(addressMapFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new addressMap
    public ResponseEntity<Object> createAddressMap(CAddressMap pAddressMap){
        try {
            CAddressMap newAddressMap = new CAddressMap();

                newAddressMap.setAddress(pAddressMap.getAddress());
                newAddressMap.setLat(pAddressMap.getLat());
                newAddressMap.setLng(pAddressMap.getLng());

                CAddressMap savedAddressMap = addressMapRepository.save(newAddressMap);

                return new ResponseEntity<>(savedAddressMap, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified street: " + e.getCause().getCause().getMessage());
        }
    }

    //Update a addressMap by Id
    public ResponseEntity<Object> updateAddressMapById(int id, CAddressMap pAddressMap){
        try {
            Optional<CAddressMap> addressMapData = addressMapRepository.findById(id);

            if (addressMapData.isPresent()){

                CAddressMap addressMapFound =  addressMapData.get();

                addressMapFound.setAddress(pAddressMap.getAddress());
                addressMapFound.setLat(pAddressMap.getLat());
                addressMapFound.setLng(pAddressMap.getLng());

                return new ResponseEntity<>(addressMapRepository.save(addressMapFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete addressMap by id
    public ResponseEntity<Object> deleteAddressMapById(int id){
        try {
            Optional<CAddressMap> addressMapData = addressMapRepository.findById(id);
            if(addressMapData.isPresent()){
                addressMapRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
