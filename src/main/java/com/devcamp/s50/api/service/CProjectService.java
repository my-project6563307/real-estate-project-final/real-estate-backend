package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CConstructionContractor;
import com.devcamp.s50.api.model.CDesignUnit;
import com.devcamp.s50.api.model.CDistrict;
import com.devcamp.s50.api.model.CInvestor;
import com.devcamp.s50.api.model.CProject;
import com.devcamp.s50.api.model.CProvince;
import com.devcamp.s50.api.model.CRegionLink;
import com.devcamp.s50.api.model.CStreet;
import com.devcamp.s50.api.model.CUtilities;
import com.devcamp.s50.api.model.CWard;
import com.devcamp.s50.api.repository.IConstructionContractorRepository;
import com.devcamp.s50.api.repository.IDesignUnitRepository;
import com.devcamp.s50.api.repository.IDistrictRepository;
import com.devcamp.s50.api.repository.IInvestorRepository;
import com.devcamp.s50.api.repository.IProjectRepository;
import com.devcamp.s50.api.repository.IProvinceRepository;
import com.devcamp.s50.api.repository.IRegionLinkRepository;
import com.devcamp.s50.api.repository.IStreetRepository;
import com.devcamp.s50.api.repository.IUtilitiesRepository;
import com.devcamp.s50.api.repository.IWardRepository;

@Service
public class CProjectService {
    @Autowired
    IProjectRepository projectRepository;
    @Autowired
    IConstructionContractorRepository constRepo;
    @Autowired
    IDesignUnitRepository designUnitRepository;
    @Autowired
    IInvestorRepository investorRepository;
    @Autowired
    IRegionLinkRepository regionLinkRepository;
    @Autowired
    IUtilitiesRepository utilitiesRepository;
    @Autowired
    IProvinceRepository provinceRepository;
    @Autowired
    IDistrictRepository districtRepository;
    @Autowired
    IWardRepository wardRepository;
    @Autowired
    IStreetRepository streetRepository;

    //get all projects
    public ResponseEntity<List<CProject>> getAllProject(String page, String size){
            try {
                Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

                List<CProject> projectList = new ArrayList<>();
                projectRepository.findAll(pageWithElements).forEach(projectList::add);
                
                return new ResponseEntity<>(projectList, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
    }

    //get project by id
    public ResponseEntity<Object> getProjectById (Integer id){
        try {
            Optional<CProject> projectData = projectRepository.findById(id);
            if(projectData.isPresent()){

                CProject projectFound = projectData.get();

                return new ResponseEntity<>(projectFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a project
    public ResponseEntity<Object> createProject(CProject pProject, Integer constructionId,
            Integer designUnitId, Integer investorId, Integer regionLinkId, Integer utilitiesId,
            Integer provinceId,Integer districtId, Integer wardId, Integer streetId){
        try {
            Optional<CConstructionContractor> construcData = constRepo.findById(constructionId);
            Optional<CDesignUnit> designUnitData = designUnitRepository.findById(designUnitId);
            Optional<CInvestor> investorData = investorRepository.findById(investorId);
            Optional<CRegionLink> regionLinkData = regionLinkRepository.findById(regionLinkId);
            Optional<CUtilities> utilitiesData = utilitiesRepository.findById(utilitiesId);
            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            Optional<CWard> wardData = wardRepository.findById(wardId);
            Optional<CStreet> streetData = streetRepository.findById(streetId);

            CProject newProject = new CProject();

            newProject.setAcreage(pProject.getAcreage());
            newProject.setAddress(pProject.getAddress());
            newProject.setApartmenttArea(pProject.getApartmenttArea());
            newProject.setConstructArea(pProject.getConstructArea());
            newProject.setDescription(pProject.getDescription());
            newProject.setLat(pProject.getLat());
            newProject.setLng(pProject.getLng());
            newProject.setName(pProject.getName());
            newProject.setNumApartment(pProject.getNumApartment());
            newProject.setNumBlock(pProject.getNumBlock());
            newProject.setNumFloors(pProject.getNumFloors());
            newProject.setPhoto(pProject.getPhoto());
            newProject.setSlogan(pProject.getSlogan());
            newProject.setConstructionContractor(construcData.get());
            newProject.setDesignUnit(designUnitData.get());
            newProject.setInvestor(investorData.get());
            newProject.setRegionLink(regionLinkData.get());
            newProject.setUtilities(utilitiesData.get());
            newProject.setProvince(provinceData.get());
            newProject.setDistrict(districtData.get());
            newProject.setWard(wardData.get());
            newProject.setStreet(streetData.get());

            CProject savedProject = projectRepository.save(newProject);

            return new ResponseEntity<>(savedProject, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Project: " + e.getCause().getCause().getMessage());
        }
    }

    //Update a project by id
    public ResponseEntity<Object> updateProjectById(Integer id, CProject pProject, Integer constructionId,
    Integer designUnitId, Integer investorId, Integer regionLinkId, Integer utilitiesId,
    Integer provinceId,Integer districtId, Integer wardId, Integer streetId){
        try {
            Optional<CProject> projectData = projectRepository.findById(id);

            if (projectData.isPresent()){

                CProject projectFound =  projectData.get();

                Optional<CConstructionContractor> construcData = constRepo.findById(constructionId);
                Optional<CDesignUnit> designUnitData = designUnitRepository.findById(designUnitId);
                Optional<CInvestor> investorData = investorRepository.findById(investorId);
                Optional<CRegionLink> regionLinkData = regionLinkRepository.findById(regionLinkId);
                Optional<CUtilities> utilitiesData = utilitiesRepository.findById(utilitiesId);
                Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
                Optional<CDistrict> districtData = districtRepository.findById(districtId);
                Optional<CWard> wardData = wardRepository.findById(wardId);
                Optional<CStreet> streetData = streetRepository.findById(streetId);

                projectFound.setAcreage(pProject.getAcreage());
                projectFound.setAddress(pProject.getAddress());
                projectFound.setApartmenttArea(pProject.getApartmenttArea());
                projectFound.setConstructArea(pProject.getConstructArea());
                projectFound.setDescription(pProject.getDescription());
                projectFound.setLat(pProject.getLat());
                projectFound.setLng(pProject.getLng());
                projectFound.setName(pProject.getName());
                projectFound.setNumApartment(pProject.getNumApartment());
                projectFound.setNumBlock(pProject.getNumBlock());
                projectFound.setNumFloors(pProject.getNumFloors());
                projectFound.setPhoto(pProject.getPhoto());
                projectFound.setSlogan(pProject.getSlogan());
                projectFound.setConstructionContractor(construcData.get());
                projectFound.setDesignUnit(designUnitData.get());
                projectFound.setInvestor(investorData.get());
                projectFound.setRegionLink(regionLinkData.get());
                projectFound.setUtilities(utilitiesData.get());
                projectFound.setProvince(provinceData.get());
                projectFound.setDistrict(districtData.get());
                projectFound.setWard(wardData.get());
                projectFound.setStreet(streetData.get());
                    
                return new ResponseEntity<>(projectRepository.save(projectFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //delete project by id
    public ResponseEntity<Object> deleteProjectById(Integer id){
        try {
            Optional<CProject> projectData = projectRepository.findById(id);
            if(projectData.isPresent()){

                projectRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
