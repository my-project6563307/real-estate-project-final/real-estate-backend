package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CLocations;
import com.devcamp.s50.api.repository.ILocationsRepository;

@Service
public class CLocationsService {
    @Autowired
    ILocationsRepository locationsRepository;

    //Get all locations
    public ResponseEntity<List<CLocations>> getAllLocations(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CLocations> locationsList = new ArrayList<>();
            
            locationsRepository.findAll(pageWithElements).forEach(locationsList::add);

            return new ResponseEntity<>(locationsList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Get Location by Location id
    public ResponseEntity<Object> getLocationById (Integer id){
        try {
            Optional<CLocations> locationsData = locationsRepository.findById(id);
            if(locationsData.isPresent()){

                CLocations locationsFound = locationsData.get();

                return new ResponseEntity<>(locationsFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new Location
    public ResponseEntity<Object> createLocation(CLocations pLocations){
        try {

            CLocations newLocation = new CLocations();

            newLocation.setLatitude(pLocations.getLatitude());
            newLocation.setLongitude(pLocations.getLongitude());

            CLocations savedLocations = locationsRepository.save(newLocation);

            return new ResponseEntity<>(savedLocations, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Location: " + e.getCause().getCause().getMessage());
        }
    }

    //Update location by id
    public ResponseEntity<Object> updateLocationById(Integer id, CLocations pLocations){
        try {
            Optional<CLocations> locationsData = locationsRepository.findById(id);

            if (locationsData.isPresent()){

                CLocations locationsFound =  locationsData.get();

                locationsFound.setLatitude(pLocations.getLatitude());
                locationsFound.setLongitude(pLocations.getLongitude());

                return new ResponseEntity<>(locationsRepository.save(locationsFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete location by id
    public ResponseEntity<Object> deleteLocationById(Integer id){
        try {
            Optional<CLocations> locationsData = locationsRepository.findById(id);
            if(locationsData.isPresent()){

                locationsRepository.deleteById(id);

                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
