package com.devcamp.s50.api.service;

import java.util.Optional;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CDesignUnit;
import com.devcamp.s50.api.repository.IDesignUnitRepository;

@Service
public class CDesignUnitService {
    @Autowired
    IDesignUnitRepository designUnitRepository;

    // Get all Design  Unit
    public ResponseEntity<List<CDesignUnit>> getAllDesignUnits(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CDesignUnit> designUnitList = new ArrayList<>();
            designUnitRepository.findAll(pageWithElements).forEach(designUnitList::add);

            return new ResponseEntity<>(designUnitList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Get Design Unit by id
    public ResponseEntity<Object> getDesignUnitById (int id){
        try {
            Optional<CDesignUnit> designUnitData = designUnitRepository.findById(id);
            if(designUnitData.isPresent()){

                CDesignUnit designUnitFound = designUnitData.get();

                return new ResponseEntity<>(designUnitFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new Design Unit
    public ResponseEntity<Object> createDesignUnit(CDesignUnit pDesignUnit){
        try {

            CDesignUnit newDesignUnit = new CDesignUnit();

            newDesignUnit.setName(pDesignUnit.getName());
            newDesignUnit.setDescription(pDesignUnit.getDescription());
            newDesignUnit.setProjects(pDesignUnit.getProjects());
            newDesignUnit.setAddress(pDesignUnit.getAddress());
            newDesignUnit.setPhone(pDesignUnit.getPhone());
            newDesignUnit.setPhone2(pDesignUnit.getPhone2());
            newDesignUnit.setFax(pDesignUnit.getFax());
            newDesignUnit.setEmail(pDesignUnit.getEmail());
            newDesignUnit.setWebsite(pDesignUnit.getWebsite());
            newDesignUnit.setNote(pDesignUnit.getNote());

            CDesignUnit savedDesignUnit = designUnitRepository.save(newDesignUnit);

            return new ResponseEntity<>(savedDesignUnit, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Design unit: " + e.getCause().getCause().getMessage());
        }
    }

    //Update Design Unit by Design Unit id
    public ResponseEntity<Object> updateDesignUnitById(int id, CDesignUnit pDesignUnit){
        try {
            Optional<CDesignUnit> designUnitData = designUnitRepository.findById(id);

            if (designUnitData.isPresent()){

                CDesignUnit designUnitFound =  designUnitData.get();

                designUnitFound.setName(pDesignUnit.getName());
                designUnitFound.setDescription(pDesignUnit.getDescription());
                designUnitFound.setProjects(pDesignUnit.getProjects());
                designUnitFound.setAddress(pDesignUnit.getAddress());
                designUnitFound.setPhone(pDesignUnit.getPhone());
                designUnitFound.setPhone2(pDesignUnit.getPhone2());
                designUnitFound.setFax(pDesignUnit.getFax());
                designUnitFound.setEmail(pDesignUnit.getEmail());
                designUnitFound.setWebsite(pDesignUnit.getWebsite());
                designUnitFound.setNote(pDesignUnit.getNote());

                return new ResponseEntity<>(designUnitRepository.save(designUnitFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete design Unit by id
    public ResponseEntity<Object> deleteDesignUnitById(int id){
        try {
            Optional<CDesignUnit> designUnitData = designUnitRepository.findById(id);
            if(designUnitData.isPresent()){
                designUnitRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
