package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CDistrict;
import com.devcamp.s50.api.model.CProvince;
import com.devcamp.s50.api.model.CStreet;
import com.devcamp.s50.api.repository.IDistrictRepository;
import com.devcamp.s50.api.repository.IProvinceRepository;
import com.devcamp.s50.api.repository.IStreetRepository;

@Service
public class CStreetService {
    @Autowired
    IStreetRepository streetRepository;
    @Autowired
    IDistrictRepository districtRepository;
    @Autowired
    IProvinceRepository provinceRepository;

    //Get all streets
    public ResponseEntity<List<CStreet>> getAllStreets(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CStreet> streetList = new ArrayList<>();
            streetRepository.findAll(pageWithElements).forEach(streetList::add);

            return new ResponseEntity<>(streetList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Get Street by street id
    public ResponseEntity<Object> getStreetByStreetId (Integer streetId){
        try {
            Optional<CStreet> streetData = streetRepository.findById(streetId);
            if(streetData.isPresent()){
                CStreet streetFound = streetData.get();

                return new ResponseEntity<>(streetFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Get street by district id
    public ResponseEntity<Object> getStreetByDistrictId (Integer districtId){
        try {
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            if(districtData.isPresent()){
                CDistrict districtFound = districtData.get();
                Set<CStreet> streetFound = districtFound.getStreets();

                return new ResponseEntity<>(streetFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Get street by province id
    public ResponseEntity<Object> getStreetByProvinceId (Integer provinceId){
        try {
            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
            if(provinceData.isPresent()){
                CProvince provinceFound = provinceData.get();
                Set<CStreet> streetFound = provinceFound.getStreets();

                return new ResponseEntity<>(streetFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create new street with province id and districtid
    public ResponseEntity<Object> createStreet(Integer provinceId, Integer districtId, CStreet pStreet){
        try {
            Optional<CProvince> provinceData =  provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);

            if (provinceData.isPresent() && districtData.isPresent()){

                CProvince provinceFound = provinceData.get();
                CDistrict districtFound = districtData.get();

                CStreet newStreet = new CStreet();

                newStreet.setPrefix(pStreet.getPrefix());
                newStreet.setName(pStreet.getName());
                newStreet.setDistrict(districtFound);
                newStreet.setProvince(provinceFound);

                CStreet savedStreet = streetRepository.save(newStreet);

                return new ResponseEntity<>(savedStreet, HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified street: " + e.getCause().getCause().getMessage());
        }
    }

    //Update street by street id
    public ResponseEntity<Object> updateStreetByStreetId(Integer provinceId, Integer districtId,Integer streetId, CStreet pStreet){
        try {
            Optional<CProvince> provinceData =  provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            Optional<CStreet> streetData = streetRepository.findById(streetId);

            if (streetData.isPresent()){

                CStreet streetFound =  streetData.get();

                streetFound.setPrefix(pStreet.getPrefix());
                streetFound.setName(pStreet.getName());
                streetFound.setDistrict(districtData.get());
                streetFound.setProvince(provinceData.get());
                return new ResponseEntity<>(streetRepository.save(streetFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete street by id
    public ResponseEntity<Object> deleteStreetByStreetId(Integer streetId){
        try {
            Optional<CStreet> streetData = streetRepository.findById(streetId);
            if(streetData.isPresent()){
                streetRepository.deleteById(streetId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
