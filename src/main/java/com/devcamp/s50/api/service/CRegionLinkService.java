package com.devcamp.s50.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.api.model.CRegionLink;
import com.devcamp.s50.api.repository.IRegionLinkRepository;

@Service
public class CRegionLinkService {
    @Autowired
    IRegionLinkRepository regionLinkRepository;

    //Get all region links
    public ResponseEntity<List<CRegionLink>> getAllRegionLinks(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CRegionLink> regionLinkList = new ArrayList<>();
            regionLinkRepository.findAll(pageWithElements).forEach(regionLinkList::add);

            return new ResponseEntity<>(regionLinkList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Get Region link by Id;
    public ResponseEntity<Object> getRegionLinkById (Integer id){
        try {
            Optional<CRegionLink> regionLinkData = regionLinkRepository.findById(id);
            if(regionLinkData.isPresent()){

                CRegionLink regionLinkFound = regionLinkData.get();

                return new ResponseEntity<>(regionLinkFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a Region Link
    public ResponseEntity<Object> createRegionLink(CRegionLink pRegionLink){
        try {

            CRegionLink newRegionLink = new CRegionLink();

            newRegionLink.setName(pRegionLink.getName());
            newRegionLink.setDescription(pRegionLink.getDescription());
            newRegionLink.setPhoto(pRegionLink.getPhoto());
            newRegionLink.setAddress(pRegionLink.getAddress());
            newRegionLink.setLat(pRegionLink.getLat());
            newRegionLink.setLng(pRegionLink.getLng());

            CRegionLink savedRegionLink = regionLinkRepository.save(newRegionLink);

            return new ResponseEntity<>(savedRegionLink, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Region link: " + e.getCause().getCause().getMessage());
        }
    }

    // Update Region link by id
    public ResponseEntity<Object> updateDesignUnitById(Integer id, CRegionLink pRegionLink){
        try {
            Optional<CRegionLink> regionLinkData = regionLinkRepository.findById(id);

            if (regionLinkData.isPresent()){

                CRegionLink regionLinkFound =  regionLinkData.get();

                regionLinkFound.setName(pRegionLink.getName());
                regionLinkFound.setDescription(pRegionLink.getDescription());
                regionLinkFound.setPhoto(pRegionLink.getPhoto());
                regionLinkFound.setAddress(pRegionLink.getAddress());
                regionLinkFound.setLat(pRegionLink.getLat());
                regionLinkFound.setLng(pRegionLink.getLng());

                return new ResponseEntity<>(regionLinkRepository.save(regionLinkFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete region link by id
    public ResponseEntity<Object> deleteRegionLinkById(Integer id){
        try {
            Optional<CRegionLink> regionLinkData = regionLinkRepository.findById(id);
            if(regionLinkData.isPresent()){
                regionLinkRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
