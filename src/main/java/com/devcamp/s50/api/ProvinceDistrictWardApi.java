package com.devcamp.s50.api;

// import java.util.HashSet;
// import java.util.Set;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.security.crypto.password.PasswordEncoder;

// import com.devcamp.s50.api.model.CEmployees;
// import com.devcamp.s50.api.model.ERole;
// import com.devcamp.s50.api.model.Role;
// import com.devcamp.s50.api.repository.IEmployeesRepository;
// import com.devcamp.s50.api.repository.RoleRepository;


@SpringBootApplication
public class ProvinceDistrictWardApi{

	public static void main(String[] args) {
		SpringApplication.run(ProvinceDistrictWardApi.class, args);
	}

	// @Autowired implements CommandLineRunner
	// RoleRepository roleRepository;

	// @Autowired
	// IEmployeesRepository employeesRepository;

	// @Autowired
	// PasswordEncoder encoder;

	// @Override
	// public void run(String... params) throws Exception {
	// 	if(!roleRepository.findByName(ERole.ROLE_CUSTOMER).isPresent()) {
	// 		roleRepository.save(new Role(ERole.ROLE_CUSTOMER));
	// 	}

	// 	if(!roleRepository.findByName(ERole.ROLE_HOME_SELLER).isPresent()) {
	// 		roleRepository.save(new Role(ERole.ROLE_HOME_SELLER));
	// 	}

	// 	if(!roleRepository.findByName(ERole.ROLE_ADMIN).isPresent()) {
	// 		roleRepository.save(new Role(ERole.ROLE_ADMIN));
	// 	}

	// 	CEmployees initEmployee = new CEmployees();
	// 	initEmployee.setUsername("chuongnv");
	// 	initEmployee.setEmail("chuongnv@gmail.com");
	// 	initEmployee.setPassword(encoder.encode("12345"));

	// 	Set<Role> roles = new HashSet<>();
	// 	Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	// 	roles.add(userRole);
	// 	initEmployee.setRoles(roles);

	// 	if(!employeesRepository.existsByEmail("chuongnv@gmail.com")) {
	// 		employeesRepository.save(initEmployee);
	// 	}
	// }
}
