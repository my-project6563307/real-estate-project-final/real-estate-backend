package com.devcamp.s50.api.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CDesignUnit;
import com.devcamp.s50.api.service.CDesignUnitService;

@RestController
@RequestMapping("/")
@CrossOrigin
@PreAuthorize("hasRole ('ADMIN')")
public class CDesignUnitController {
    @Autowired
    CDesignUnitService designUnitService;

    @GetMapping("design_units")
    public ResponseEntity<List<CDesignUnit>> getAllDesignUnits(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return designUnitService.getAllDesignUnits(page, size);
    }

    @GetMapping("design_units/{id}")
    public ResponseEntity<Object> getDesignUnitById (@PathVariable int id){
        return designUnitService.getDesignUnitById(id);
    }

    @PostMapping("design_units")
    public ResponseEntity<Object> createDesignUnit(@RequestBody CDesignUnit pDesignUnit){
        return designUnitService.createDesignUnit(pDesignUnit);
    }

    @PutMapping("design_units/{id}")
    public ResponseEntity<Object> updateDesignUnitById(@PathVariable int id,@RequestBody CDesignUnit pDesignUnit){
        return designUnitService.updateDesignUnitById(id, pDesignUnit);
    }

    @DeleteMapping("design_units/{id}")
    public ResponseEntity<Object> deleteDesignUnitById(@PathVariable int id){
        return designUnitService.deleteDesignUnitById(id);
    }
}
