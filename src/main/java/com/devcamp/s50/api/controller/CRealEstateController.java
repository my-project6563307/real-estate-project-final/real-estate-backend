package com.devcamp.s50.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CRealEstate;
import com.devcamp.s50.api.repository.IRealEstateRepository;
import com.devcamp.s50.api.service.CRealEstateService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CRealEstateController {
    @Autowired
    CRealEstateService realEstateService;
    @Autowired
    IRealEstateRepository realEstateRepository;

    //Get all Real estate {only admin has authorization}
    @GetMapping("real_estates")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<List<CRealEstate>> getAllRealEstate(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return realEstateService.getAllRealEstate(page, size);
    }
    //Get all Real estate is Posted
    @GetMapping("api/test/real_estates/posted")
    public ResponseEntity<List<CRealEstate>> getPostedRealEstate(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "20") String size){
        return realEstateService.getPostedRealEstate(page, size);
    }
    //Get all Real estate is Posted by id
    @GetMapping("api/test/real_estates/posted/{id}")
    public ResponseEntity<CRealEstate> getPostedRealEstateById(@PathVariable Integer id){
        return realEstateService.getPostedRealEstateById(id);
    }

    //get Estates by id
    @GetMapping("real_estates/{id}")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public ResponseEntity<Object> getDesignUnitById (HttpServletRequest request, @PathVariable Integer id){
        return realEstateService.getDesignUnitById(request, id);
    }

    //Create a realestate
    @PostMapping("real_estates")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public ResponseEntity<Object> createRealEstate(HttpServletRequest request, @RequestBody CRealEstate pRealEstate,
    @RequestParam Integer provinceId,
    @RequestParam Integer districtId,@RequestParam Integer wardId,
    @RequestParam Integer streetId,@RequestParam Integer projectId){
        return realEstateService.createRealEstate(request, pRealEstate, provinceId, districtId, wardId, streetId, projectId);
    }

    //Get real estate list by request with jwt token
    @GetMapping("real_estates/current-employee")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public List<?> getRealEstateByRequest(HttpServletRequest request){
        return realEstateService.getRealEstateByRequest(request);
    }
    

    //Update a realestate by id for admin
    @PutMapping("real_estates/{id}")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<Object> updateRealEstateById(HttpServletRequest request, @PathVariable Integer id,
    @RequestBody CRealEstate pCRealEstate,
    @RequestParam Integer provinceId,
    @RequestParam Integer districtId,@RequestParam Integer wardId,
    @RequestParam Integer streetId,@RequestParam Integer projectId,
    @RequestParam Long employeeId){

        return realEstateService.updateRealEstateById(request, id,pCRealEstate, provinceId, districtId, wardId, streetId, projectId, employeeId);
    }


    //Delete realestate by id and request
    @DeleteMapping("real_estates/{id}")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public ResponseEntity<Object> deleteRealEstateById(HttpServletRequest request, @PathVariable Integer id){
        return realEstateService.deleteRealEstateById(request,id);
    }

    // get Estates by query searching
    @GetMapping("api/test/real_estates/find/request/{request}/province/{provinceId}/price1/{price1}/price2/{price2}/acreage1/{acreage1}/acreage2/{acreage2}")
    public List<CRealEstate> findByReques(@PathVariable String request, @PathVariable String provinceId, 
    @PathVariable String price1,@PathVariable String price2,@PathVariable String acreage1,@PathVariable String acreage2){
            System.out.println(request.equals("-1"));
            if (request.equals("-1")){
                request = "%";
            }
            if(provinceId.equals("-1")){
                provinceId = "%";
            }
            return realEstateRepository.findByRequest(request,provinceId, price1,price2, acreage1, acreage2 );
        }
    
    //Update real estate for home seller
    @PutMapping("real_estates/seller-role/{realestateId}")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('HOME_SELLER')")
    public ResponseEntity<Object> updateRealEstateForSellerRole(HttpServletRequest request,@PathVariable Integer realestateId,@RequestBody CRealEstate pRealEstate,
    @RequestParam Integer provinceId,@RequestParam Integer districtId,@RequestParam Integer wardId,
    @RequestParam Integer streetId,@RequestParam Integer projectId){
        return realEstateService.updateRealEstateForSellerRole(request, realestateId, pRealEstate, provinceId, districtId, wardId, streetId, projectId);
    }

    //Update real estate for customer
    @PutMapping("real_estates/customer-role/{realestateId}")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER')")
    public ResponseEntity<Object> updateRealEstateForCustomerRole(HttpServletRequest request,@PathVariable Integer realestateId,@RequestBody CRealEstate pRealEstate,
    @RequestParam Integer provinceId,@RequestParam Integer districtId,@RequestParam Integer wardId,
    @RequestParam Integer streetId,@RequestParam Integer projectId){
        return realEstateService.updateRealEstateForCustomerRole(request, realestateId, pRealEstate, provinceId, districtId, wardId, streetId, projectId);
    }
}
