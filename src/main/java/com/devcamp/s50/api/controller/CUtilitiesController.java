package com.devcamp.s50.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CUtilities;
import com.devcamp.s50.api.service.CUtilitiesService;

@RestController
@RequestMapping("/")
@CrossOrigin
@PreAuthorize("hasRole ('ADMIN')")
public class CUtilitiesController {
    @Autowired
    CUtilitiesService utilitiesService;

    //Get all Utilities
    @GetMapping("utilities")
    public ResponseEntity<List<CUtilities>> getAllUtilities(
    @RequestParam(defaultValue = "0") String page,
    @RequestParam(defaultValue = "50") String size){
        return utilitiesService.getAllUtilities(page, size);
    }

    // Get Utitlities By Id
    @GetMapping("utilities/{id}")
    public ResponseEntity<Object> getUtilitiesById (@PathVariable Integer id){
        return utilitiesService.getUtilitiesById(id);
    }

    // Create a new Utilities
    @PostMapping("utilities")
    public ResponseEntity<Object> createUtilities(@RequestBody CUtilities pUtilities){
        return utilitiesService.createUtilities(pUtilities);
    }

    //Update Design Unit by Design Unit id
    @PutMapping("utilities/{id}")
    public ResponseEntity<Object> updateUtilitiesById(@PathVariable Integer id,@RequestBody CUtilities pUtilities){
        return utilitiesService.updateUtilitiesById(id, pUtilities);
    }

    //delete design Unit by id
    @DeleteMapping("utilities/{id}")
    public ResponseEntity<Object> deleteUtilitiesById(@PathVariable Integer id){
        return utilitiesService.deleteUtilitiesById(id);
    }
}
