package com.devcamp.s50.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CStreet;
import com.devcamp.s50.api.service.CStreetService;

@RestController
@RequestMapping("/")
@CrossOrigin
@PreAuthorize("hasRole ('ADMIN')")
public class CStreetController {
    @Autowired
    CStreetService streetService;

    // Get all streets
    @GetMapping("streets")
    public ResponseEntity<List<CStreet>> getAllStreets(
    @RequestParam(defaultValue = "0") String page,
    @RequestParam(defaultValue = "50") String size){
        return streetService.getAllStreets(page, size);
    }
    
    //Get Street by street id
    @GetMapping("streets/{streetId}")
    public ResponseEntity<Object> getStreetByStreetById (@PathVariable Integer streetId){
        return streetService.getStreetByStreetId(streetId);
    }

    //Get street by district id
    @GetMapping("streets/district/{districtId}")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public ResponseEntity<Object> getStreetByDistrictId (@PathVariable Integer districtId){
        return streetService.getStreetByDistrictId(districtId);
    }

    //Get street by province id
    @GetMapping("streets/province/{provinceId}")
    public ResponseEntity<Object> getStreetByProvinceId (@PathVariable Integer provinceId){
        return streetService.getStreetByProvinceId(provinceId);
    }

    // Create new street with province id and districtid
    @PostMapping("streets/district/{districtId}/province/{provinceId}")
    public ResponseEntity<Object> createStreet(@PathVariable Integer provinceId,
    @PathVariable Integer districtId,@RequestBody CStreet pStreet){
        return streetService.createStreet(provinceId, districtId, pStreet);
    }

    //Update street by street id
    @PutMapping("streets/{streetId}")
    public ResponseEntity<Object> updateStreetByStreetId(@RequestParam Integer provinceId,
    @RequestParam Integer districtId, @PathVariable Integer streetId,@RequestBody CStreet pStreet){
        return streetService.updateStreetByStreetId(provinceId, districtId, streetId, pStreet);
    }

    //delete street by id
    @DeleteMapping("streets/{streetId}")
    public ResponseEntity<Object> deleteStreetByStreetId(Integer streetId){
        return streetService.deleteStreetByStreetId(streetId);
    }
}
