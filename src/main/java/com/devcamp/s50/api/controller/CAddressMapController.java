package com.devcamp.s50.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CAddressMap;
import com.devcamp.s50.api.service.CAddressMapService;

@RestController
@RequestMapping("/")
@CrossOrigin
@PreAuthorize("hasRole ('ADMIN')")
public class CAddressMapController {
    @Autowired
    CAddressMapService addressMapService;

    @GetMapping("address_map")
    public ResponseEntity<List<CAddressMap>> getAllAddressMap(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return addressMapService.getAllAddressMap( page, size);
    }

    @GetMapping("address_map/{id}")
    public ResponseEntity<Object> getAddressMapById (@PathVariable int id){
        return addressMapService.getAddressMapById(id);
    }

    @PostMapping("address_map")
    public ResponseEntity<Object> createAddressMap(@RequestBody CAddressMap pAddressMap){
        return addressMapService.createAddressMap(pAddressMap);
    }

    @PutMapping("address_map/{id}")
    public ResponseEntity<Object> updateAddressMapById(@PathVariable int id, @RequestBody CAddressMap pAddressMap){
        return addressMapService.updateAddressMapById(id, pAddressMap);
    }

    @DeleteMapping("address_map/{id}")
    public ResponseEntity<Object> deleteAddressMapById(@PathVariable int id){
        return addressMapService.deleteAddressMapById(id);
    }
}
