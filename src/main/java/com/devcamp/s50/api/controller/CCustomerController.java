package com.devcamp.s50.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CCustomer;
import com.devcamp.s50.api.service.CCustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CCustomerController {
    @Autowired
    CCustomerService customerService;
    @GetMapping("customers")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<List<CCustomer>> getAllCustomers(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return customerService.getAllCustomers(page, size);
    }

    @GetMapping("customers/{id}")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<Object> getCustomerById (@PathVariable int id){
        return customerService.getCustomerById(id);
    }

    // Cho phép toàn bộ người dùng gửi thông tin đăng kí lên server
    @PostMapping("api/test/customers")
    public ResponseEntity<Object> createCustomer(@RequestBody CCustomer pCustomer){
        return customerService.createCustomer(pCustomer);
    }

    @PutMapping("customers/{id}")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<Object> updateteCustomer(@PathVariable int id,@RequestBody CCustomer pCustomer){
        return customerService.updateteCustomer(id, pCustomer);
    }

    @DeleteMapping("customers/{id}")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id){
        return customerService.deleteCustomerById(id);
    }
}
