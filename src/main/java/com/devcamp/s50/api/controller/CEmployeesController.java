package com.devcamp.s50.api.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CEmployees;
import com.devcamp.s50.api.model.ERole;
import com.devcamp.s50.api.model.Role;
import com.devcamp.s50.api.payload.request.LoginRequest;
import com.devcamp.s50.api.payload.request.SignupRequest;
import com.devcamp.s50.api.payload.response.JwtResponse;
import com.devcamp.s50.api.payload.response.MessageResponse;
import com.devcamp.s50.api.repository.IEmployeesRepository;
import com.devcamp.s50.api.repository.RoleRepository;
import com.devcamp.s50.api.security.jwt.JwtUtils;
import com.devcamp.s50.api.security.services.UserDetailsImpl;
import com.devcamp.s50.api.security.services.UserDetailsServiceImpl;
import com.devcamp.s50.api.service.CEmployeesService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CEmployeesController {
    @Autowired
    CEmployeesService employeesService;

    @Autowired

    IEmployeesRepository employeesRepository;

    @Autowired
    AuthenticationManager authenticationManager;
  
    @Autowired
    RoleRepository roleRepository;
  
    @Autowired
    PasswordEncoder encoder;
  
    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;
    // Đăng nhập vào hệ thống nhận mã jwt.
    @PostMapping("api/auth/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();    
        List<String> roles = userDetails.getAuthorities().stream()
            .map(item -> item.getAuthority())
            .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt, 
                            userDetails.getId(), 
                            userDetails.getUsername(), 
                            userDetails.getEmail(), 
                            roles));
    }
  
    // Đăng kí người dùng mới
    @PostMapping("api/test/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        // Kiểm tra người dùng có tồn tại hay chưa
        if (employeesRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (employeesRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Error: Email is already in use!"));
        }
        // Tạo thông tin người dùng
        CEmployees employee = new CEmployees();
        employee.setUsername(signUpRequest.getUsername());
        employee.setEmail(signUpRequest.getEmail());
        employee.setPassword(encoder.encode(signUpRequest.getPassword()));

        // Người dùng mới luôn có role là customer
        Optional<Role> userRole = roleRepository.findByName(ERole.ROLE_CUSTOMER);

        Set<Role> roles = new HashSet<>();

        roles.add(userRole.get());

        employee.setRoles(roles);

        // Lưu vào kho người dùng
        employeesRepository.saveAndFlush(employee);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    //  Lấy danh sách người dùng (Chỉ admin có quyền này)
    @GetMapping("employees")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<CEmployees>> getAllEmployees(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return employeesService.getAllEmployees(page, size);
    }
    
    // Lấy thông tin người dùng qua với request kèm mã jwt, kiểm tra mã jwt còn hiệu lực hay không
    @GetMapping("/employees/me")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('HOME_SELLER') or hasRole('ADMIN')")
    public CEmployees getEmployeeByRequest(HttpServletRequest request) {
        return userDetailsServiceImpl.whoami(request);
    }

    // Cập nhật thông tin người dùng với request kèm mã jwt
    @PutMapping("/employees/me")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('HOME_SELLER') or hasRole('ADMIN')")
    public CEmployees updateEmployeeByRequest(HttpServletRequest request, @RequestBody CEmployees pEmployee ) {
        return employeesService.updateEmployeeByRequest(request, pEmployee);
    }

    // Xem thông tin người dùng qua id {chỉ admin có quyền này}
    @GetMapping("employees/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<CEmployees> getDesignUnitById (@PathVariable Long id){
        return employeesService.getEmployeesById(id);
    }

    //Thêm mới trực tiếp người dùng { chỉ admin có quyền này}
    @PostMapping("employees")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createEmployees(@RequestBody CEmployees pEmployees){
        return employeesService.createEmployees(pEmployees);
    }

    // Cập nhật thông tin người dùng qua id {chỉ admin có quyền này}
    @PutMapping("employees/{id}/role/{roleId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateEmployeesById(@PathVariable Long id, @PathVariable Long roleId, @RequestBody CEmployees pEmployees){
        return employeesService.updateEmployeesById(id, pEmployees, roleId);
    }

    // Xoá người dùng qua id { chỉ admin có quyền này}
    @DeleteMapping("employees/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteEmployeesById(@PathVariable Long id){
        return employeesService.deleteEmployeesById(id);
    }

    // Cập nhật mật khấu qua request
    @PutMapping("employees/old-password/{oldPassword}/new-password/{newPassword}")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('HOME_SELLER') or hasRole('ADMIN')")
    public ResponseEntity<String> updateEmployeePasswordByRequest(HttpServletRequest request,
    @PathVariable String oldPassword,@PathVariable String newPassword){
        return employeesService.updateEmployeePasswordByRequest(request, oldPassword, newPassword);
    }
}
