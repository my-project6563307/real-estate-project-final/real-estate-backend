package com.devcamp.s50.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CConstructionContractor;
import com.devcamp.s50.api.service.CConstructionContractorService;

@RestController
@RequestMapping("/")
@CrossOrigin
@PreAuthorize("hasRole ('ADMIN')")
public class CConstructionContractorController {
    @Autowired
    CConstructionContractorService constructionContractorService;
    // get all construction contractor
    @GetMapping("contractors")
    public ResponseEntity<List<CConstructionContractor>> getAllConstructionContractors(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return constructionContractorService.getAllConstructionContractors(page, size);
    }
    // get construction contractor by id
    @GetMapping("contractors/{id}")
    public ResponseEntity<Object> getConstructionContractorById (@PathVariable int id){
        return constructionContractorService.getConstructionContractorById(id);
    }
    // create construction contractor
    @PostMapping("contractors")
    public ResponseEntity<Object> createConstructionContractor(@Valid @RequestBody CConstructionContractor pConstructionContractor){
        return constructionContractorService.createConstructionContractor(pConstructionContractor);
    }

    // update construction contractor by id
    @PutMapping("contractors/{id}")
    public ResponseEntity<Object> updateConstructionContractorById(@PathVariable int id,@RequestBody CConstructionContractor pConstructionContractor){
        return constructionContractorService.updateConstructionContractorById(id, pConstructionContractor);
    }
    // delete construction contractor by id
    @DeleteMapping("contractors/{id}")
    public ResponseEntity<Object> deleteConstructionContractorById(@PathVariable int id){
        return constructionContractorService.deleteConstructionContractorById(id);
    }
}
