package com.devcamp.s50.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CProvince;
import com.devcamp.s50.api.service.CProvinceService;

@RestController
@RequestMapping("/")
@CrossOrigin

public class CProvinceController {
    @Autowired
    CProvinceService pProvinceService;
    @GetMapping("api/test/provinces")
    public List<CProvince> getAllProvice(){
            return pProvinceService.getAllProvinces();
    }
    @GetMapping("provinces/{id}")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public ResponseEntity<Object> getProvinceById(@PathVariable int id){
        return pProvinceService.getProvinceById(id);
    }

    @PostMapping("provinces")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<Object> createProvince(@RequestBody CProvince pProvince){
        return pProvinceService.createProvince(pProvince);
    }
    @PutMapping("provinces/{id}")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<Object> updateProvince(@PathVariable int id,@RequestBody CProvince pProvince){
        return pProvinceService.updateProvince(id, pProvince);
    }
    @DeleteMapping("provinces/{id}")
    @PreAuthorize("hasRole ('ADMIN')")
    public ResponseEntity<Object> deleteProvince(@PathVariable int id){
        return pProvinceService.deleteProvince(id);
    }
}
