package com.devcamp.s50.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CProject;
import com.devcamp.s50.api.service.CProjectService;

@RestController
@RequestMapping("/")
@CrossOrigin
@PreAuthorize("hasRole ('ADMIN')")
public class CProjectController {
    @Autowired
    CProjectService projectService;

    @GetMapping("projects")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public ResponseEntity<List<CProject>> getAllProject(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return projectService.getAllProject(page, size);
    }

    @GetMapping("projects/{id}")
    public ResponseEntity<Object> getProjectById (@PathVariable Integer id){
        return projectService.getProjectById(id);
    }

    @PostMapping("projects")
    public ResponseEntity<Object> createProject(@RequestBody CProject pProject,@RequestParam Integer constructionId,
    @RequestParam Integer designUnitId,@RequestParam Integer investorId,@RequestParam Integer regionLinkId,@RequestParam Integer utilitiesId,
    @RequestParam Integer provinceId,@RequestParam Integer districtId,@RequestParam Integer wardId,@RequestParam Integer streetId){
        return projectService.createProject(pProject, constructionId, designUnitId,
                 investorId, regionLinkId, utilitiesId, provinceId, districtId, wardId, streetId);
    }

    @PutMapping("projects/{id}")
    public ResponseEntity<Object> updateProjectById(@PathVariable Integer id,@RequestBody CProject pProject,@RequestParam Integer constructionId,
    @RequestParam Integer designUnitId,@RequestParam Integer investorId,@RequestParam Integer regionLinkId,@RequestParam Integer utilitiesId,
    @RequestParam Integer provinceId,@RequestParam Integer districtId,@RequestParam Integer wardId,@RequestParam Integer streetId){
        return projectService.updateProjectById(id, pProject, constructionId, designUnitId, investorId,
         regionLinkId, utilitiesId, provinceId, districtId, wardId, streetId);
    }

    @DeleteMapping("projects/{id}")
    public ResponseEntity<Object> deleteProjectById(@PathVariable Integer id){
        return projectService.deleteProjectById(id);
    }
}
