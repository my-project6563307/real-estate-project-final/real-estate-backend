package com.devcamp.s50.api.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.api.model.CDistrict;
import com.devcamp.s50.api.service.CDistrictService;

@RestController
@RequestMapping("/")
@CrossOrigin
@PreAuthorize("hasRole ('ADMIN')")
public class CDistrictController {
    @Autowired
    CDistrictService pDistrictService;
    @GetMapping("districts")
    public ResponseEntity<List<CDistrict>> getAllDistricts(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return pDistrictService.getAllDistricts(page, size);
    }

    @GetMapping("districts/{id}")
    public ResponseEntity<Object> geDistrictById(@PathVariable int id){
        return pDistrictService.geDistrictById(id);
    }

    @GetMapping("districts/province/{provinceId}")
    @PreAuthorize("hasRole ('ADMIN') or hasRole ('CUSTOMER') or hasRole ('HOME_SELLER')")
    public ResponseEntity<Set<CDistrict>> getDistrictByProvinceId(@PathVariable int provinceId){
        return pDistrictService.getDistrictByProvinceId(provinceId);
    }

    @PostMapping("districts/{provinceId}")
    public ResponseEntity<Object> createDistrict(@PathVariable int provinceId,@RequestBody CDistrict pDistrict){
        return pDistrictService.createDistrict(provinceId, pDistrict);
    }

    @PutMapping("districts/{id}")
    public ResponseEntity<Object> updateDistrictById(@RequestParam int provinceId, @PathVariable int id,@RequestBody CDistrict pDistrict){
        return pDistrictService.updateDistrictById(provinceId ,id, pDistrict);
    }

    @DeleteMapping("districts/{id}")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable int id){
        return pDistrictService.deleteDistrictById(id);
    }
}
